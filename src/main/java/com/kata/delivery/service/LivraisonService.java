package com.kata.delivery.service;

import com.kata.delivery.data.model.JourLivraisonModel;
import com.kata.delivery.data.model.LivraisonModel;
import com.kata.delivery.web.dto.request.CreneauLivraisonRequestDTO;
import com.kata.delivery.web.dto.request.JourLivraisonSearchRequestDTO;
import com.kata.delivery.web.dto.request.LivraisonRequestDTO;
import com.kata.delivery.web.dto.response.CreneauLivraisonResponseDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface LivraisonService {

    Page<LivraisonModel> list(Pageable page);

    LivraisonModel get(String id);

    LivraisonModel create(LivraisonRequestDTO requestDTO);

    LivraisonModel update(String id, LivraisonRequestDTO requestDTO);

    Page<JourLivraisonModel> getCreneauLivraisonList(JourLivraisonSearchRequestDTO searchRequestDTO, Pageable page);

    CreneauLivraisonResponseDTO getCreneauLivraison(String livraisonId);

    CreneauLivraisonResponseDTO chooseCreneauLivraison(CreneauLivraisonRequestDTO requestDTO);
}
