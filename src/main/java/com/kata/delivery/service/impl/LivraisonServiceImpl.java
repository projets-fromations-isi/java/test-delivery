package com.kata.delivery.service.impl;

import com.kata.delivery.data.mapper.LivraisonMapper;
import com.kata.delivery.data.model.JourLivraisonModel;
import com.kata.delivery.data.model.LivraisonModel;
import com.kata.delivery.data.model.subclass.CreneauLivraison;
import com.kata.delivery.data.repository.JourLivraisonRepository;
import com.kata.delivery.data.repository.LivraisonRepository;
import com.kata.delivery.service.LivraisonService;
import com.kata.delivery.util.NumeroSuiviGenerator;
import com.kata.delivery.web.dto.request.CreneauLivraisonRequestDTO;
import com.kata.delivery.web.dto.request.JourLivraisonSearchRequestDTO;
import com.kata.delivery.web.dto.request.LivraisonRequestDTO;
import com.kata.delivery.web.dto.response.CreneauLivraisonResponseDTO;
import com.kata.delivery.web.exception.EntityNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;

@Service
public class LivraisonServiceImpl implements LivraisonService {

    private final LivraisonRepository livraisonRepository;

    private final JourLivraisonRepository jourLivraisonRepository;

    private final LivraisonMapper livraisonMapper;

    public LivraisonServiceImpl(LivraisonRepository livraisonRepository, JourLivraisonRepository jourLivraisonRepository,
                                LivraisonMapper livraisonMapper) {
        this.livraisonRepository = livraisonRepository;
        this.jourLivraisonRepository = jourLivraisonRepository;
        this.livraisonMapper = livraisonMapper;
    }

    @Override
    public Page<LivraisonModel> list(Pageable page) {
        return livraisonRepository.findAll(page);
    }

    @Override
    public LivraisonModel get(String id) {
        return livraisonRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("livraison.notFound"));
    }

    @Override
    public LivraisonModel create(LivraisonRequestDTO requestDTO) {
        LivraisonModel livraison = livraisonMapper.toModel(requestDTO);
        livraison.setNumeroSuivi(NumeroSuiviGenerator.genererNumeroSuivi());
        livraisonRepository.save(livraison);
        return livraison;
    }

    @Override
    public LivraisonModel update(String id, LivraisonRequestDTO requestDTO) {
        LivraisonModel livraison = livraisonRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("livraison.notFound"));
        livraisonMapper.partialUpdate(livraison, requestDTO);
        livraisonRepository.save(livraison);
        return livraison;
    }

    @Override
    public Page<JourLivraisonModel> getCreneauLivraisonList(JourLivraisonSearchRequestDTO searchRequestDTO, Pageable page) {
        return jourLivraisonRepository.search(searchRequestDTO, page);
    }

    @Override
    public CreneauLivraisonResponseDTO getCreneauLivraison(String livraisonId) {
        LivraisonModel livraison = livraisonRepository.findById(livraisonId)
                .orElseThrow(() -> new EntityNotFoundException("livraison.notFound"));

        JourLivraisonSearchRequestDTO searchRequestDTO = new JourLivraisonSearchRequestDTO();
        searchRequestDTO.setLivraisonIds(List.of(livraisonId));
        JourLivraisonModel jourLivraison = jourLivraisonRepository.search(searchRequestDTO).stream().findFirst()
                .orElseThrow(() -> new EntityNotFoundException("jourLivraison.livraison.notFound"));

        CreneauLivraison creneauLivraison = jourLivraison.getCreneauLivraisons()
                .stream().filter(creneau -> creneau.getLivraisonIds().contains(livraisonId)).findFirst().orElse(null);
        assert creneauLivraison != null;

        return new CreneauLivraisonResponseDTO(livraisonMapper.toResponseDTO(livraison), jourLivraison.getJour(),
                creneauLivraison.getHeureDebut(), creneauLivraison.getHeureFin());
    }

    @Override
    public CreneauLivraisonResponseDTO chooseCreneauLivraison(CreneauLivraisonRequestDTO requestDTO) {
        LivraisonModel livraison = livraisonRepository.findById(requestDTO.getLivraisonId())
                .orElseThrow(() -> new EntityNotFoundException("livraison.notFound"));

        JourLivraisonSearchRequestDTO searchRequestDTO = new JourLivraisonSearchRequestDTO();
        searchRequestDTO.setLivraisonIds(List.of(requestDTO.getLivraisonId()));

        if (!jourLivraisonRepository.search(searchRequestDTO).isEmpty()) {
            throw new IllegalArgumentException("livraison.creneau.alreadyExists");
        }

        JourLivraisonModel jourLivraison = jourLivraisonRepository
                .findByJour(requestDTO.getJour())
                .orElseGet(() -> new JourLivraisonModel(requestDTO.getJour()));

        CreneauLivraison creneauLivraison = jourLivraison.getCreneauLivraisons().stream()
                .filter(creneau -> creneau.getModeLivraison().equals(livraison.getModeLivraison()))
                .findFirst()
                .orElseGet(() -> new CreneauLivraison(livraison.getModeLivraison(),
                        requestDTO.getHeureDebut(), requestDTO.getHeureFin(), true, new HashSet<>()));

        creneauLivraison.addLivraisonId(livraison.getId());
        jourLivraison.addOrUpdateCreneauLivraison(creneauLivraison);

        jourLivraisonRepository.save(jourLivraison);

        return new CreneauLivraisonResponseDTO(
                livraisonMapper.toResponseDTO(livraison),
                jourLivraison.getJour(),
                creneauLivraison.getHeureDebut(),
                creneauLivraison.getHeureFin());
    }
}
