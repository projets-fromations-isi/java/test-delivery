package com.kata.delivery.data.repository;

import com.kata.delivery.data.model.JourLivraisonModel;
import com.kata.delivery.data.repository.customQuery.JourLivraisonCustomQuery;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;

@Repository
public interface JourLivraisonRepository extends MongoRepository<JourLivraisonModel, String>, JourLivraisonCustomQuery {
    Optional<JourLivraisonModel> findByJour(LocalDate jour);
}
