package com.kata.delivery.data.repository;

import com.kata.delivery.data.model.LivraisonModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LivraisonRepository extends MongoRepository<LivraisonModel, String> {
}
