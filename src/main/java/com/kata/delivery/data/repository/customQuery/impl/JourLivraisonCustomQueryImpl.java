package com.kata.delivery.data.repository.customQuery.impl;

import com.kata.delivery.data.model.JourLivraisonModel;
import com.kata.delivery.data.repository.customQuery.JourLivraisonCustomQuery;
import com.kata.delivery.web.dto.request.JourLivraisonSearchRequestDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class JourLivraisonCustomQueryImpl implements JourLivraisonCustomQuery {

    private final MongoTemplate mongoTemplate;

    public JourLivraisonCustomQueryImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public List<JourLivraisonModel> search(JourLivraisonSearchRequestDTO searchRequestDTO) {
        Query query = (new Query()).with(Sort.by(searchRequestDTO.getSortDirection(), searchRequestDTO.getSortField()));
        List<Criteria> listCriteria = getCriteria(searchRequestDTO);

        if (!listCriteria.isEmpty()) {
            query.addCriteria((new Criteria()).andOperator(listCriteria.toArray(new Criteria[0])));
        }

        return this.mongoTemplate.find(query, JourLivraisonModel.class);
    }

    @Override
    public Page<JourLivraisonModel> search(JourLivraisonSearchRequestDTO searchRequestDTO, Pageable page) {
        Query query = (new Query()).with(Sort.by(searchRequestDTO.getSortDirection(), searchRequestDTO.getSortField()));
        List<Criteria> listCriteria = getCriteria(searchRequestDTO);

        if (!listCriteria.isEmpty()) {
            query.addCriteria((new Criteria()).andOperator(listCriteria.toArray(new Criteria[0])));
        }

        List<JourLivraisonModel> jourLivraisons = this.mongoTemplate.find(query, JourLivraisonModel.class);

        return PageableExecutionUtils.getPage(jourLivraisons, page,
                () -> this.mongoTemplate.count(Query.of(query).limit(-1).skip(-1L), JourLivraisonModel.class));
    }

    private static List<Criteria> getCriteria(JourLivraisonSearchRequestDTO searchRequestDTO) {
        List<Criteria> listCriteria = new ArrayList<>();

        if (searchRequestDTO.getModeLivraison() != null) {
            listCriteria.add(Criteria.where("modeLivraison").is(searchRequestDTO.getModeLivraison()));
        }

        if (searchRequestDTO.getJour() != null) {
            listCriteria.add(Criteria.where("jour").is(searchRequestDTO.getJour()));
        }

        if (searchRequestDTO.getJourDebut() != null) {
            listCriteria.add(Criteria.where("jour").gte(searchRequestDTO.getJourDebut()));
        }

        if (searchRequestDTO.getJourFin() != null) {
            listCriteria.add(Criteria.where("jour").lte(searchRequestDTO.getJourFin()));
        }

        if (searchRequestDTO.getHeureDebut() != null) {
            listCriteria.add(Criteria.where("heureDebut").gte(searchRequestDTO.getHeureDebut()));
        }

        if (searchRequestDTO.getHeureFin() != null) {
            listCriteria.add(Criteria.where("heureFin").lte(searchRequestDTO.getHeureFin()));
        }

        if (searchRequestDTO.getLivraisonIds() != null && !searchRequestDTO.getLivraisonIds().isEmpty()) {
            listCriteria.add(Criteria.where("creneauLivraisons.livraisonIds").in(searchRequestDTO.getLivraisonIds()));
        }

        return listCriteria;
    }
}
