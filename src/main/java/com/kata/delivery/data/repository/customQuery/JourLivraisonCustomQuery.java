package com.kata.delivery.data.repository.customQuery;

import com.kata.delivery.data.model.JourLivraisonModel;
import com.kata.delivery.web.dto.request.JourLivraisonSearchRequestDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface JourLivraisonCustomQuery {
    List<JourLivraisonModel> search(JourLivraisonSearchRequestDTO searchRequestDTO);

    Page<JourLivraisonModel> search(JourLivraisonSearchRequestDTO searchRequestDTO, Pageable page);
}
