package com.kata.delivery.data.mapper;

import com.kata.delivery.data.enums.EModeLivraison;
import com.kata.delivery.data.model.JourLivraisonModel;
import com.kata.delivery.web.dto.request.LivraisonRequestDTO;
import com.kata.delivery.web.dto.response.LivraisonResponseDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface JourLivraisonMapper {

    JourLivraisonModel toModel(LivraisonRequestDTO requestDTO);

    void partialUpdate(@MappingTarget JourLivraisonModel livraisonModel, LivraisonRequestDTO requestDTO);

    default EModeLivraison mapModeLivraisonStringToEnum(String modeLivraison) {
        return EModeLivraison.valueOf(modeLivraison);
    }

    LivraisonResponseDTO toResponseDTO(JourLivraisonModel livraisonModel);
}
