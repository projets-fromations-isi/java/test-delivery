package com.kata.delivery.data.model.subclass;

import com.kata.delivery.data.enums.EModeLivraison;
import lombok.*;

import java.time.LocalTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class CreneauLivraison {

    private EModeLivraison modeLivraison;

    private LocalTime heureDebut;

    private LocalTime heureFin;

    private boolean disponible;

    private Set<String> livraisonIds = new HashSet<>();

    public CreneauLivraison() {
    }

    public CreneauLivraison(EModeLivraison modeLivraison, LocalTime heureDebut, LocalTime heureFin,
                            boolean disponible, Set<String> livraisonIds) {
        this.modeLivraison = modeLivraison;
        this.heureDebut = heureDebut;
        this.heureFin = heureFin;
        this.disponible = disponible;
        this.livraisonIds = livraisonIds;
    }

    public EModeLivraison getModeLivraison() {
        return modeLivraison;
    }

    public void setModeLivraison(EModeLivraison modeLivraison) {
        this.modeLivraison = modeLivraison;
    }

    public LocalTime getHeureDebut() {
        return heureDebut;
    }

    public void setHeureDebut(LocalTime heureDebut) {
        this.heureDebut = heureDebut;
    }

    public LocalTime getHeureFin() {
        return heureFin;
    }

    public void setHeureFin(LocalTime heureFin) {
        this.heureFin = heureFin;
    }

    public boolean isDisponible() {
        return disponible;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

    public Set<String> getLivraisonIds() {
        return livraisonIds;
    }

    public void setLivraisonIds(Set<String> livraisonIds) {
        this.livraisonIds = livraisonIds;
    }

    public void addLivraisonId(String livraisonId) {
        Set<String> updatedLivraisonIds = this.livraisonIds;
        updatedLivraisonIds.add(livraisonId);
        this.livraisonIds = updatedLivraisonIds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreneauLivraison that = (CreneauLivraison) o;
        return modeLivraison == that.modeLivraison && Objects.equals(heureDebut, that.heureDebut) && Objects.equals(heureFin, that.heureFin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(modeLivraison, heureDebut, heureFin);
    }
}
