package com.kata.delivery.data.model;

import com.kata.delivery.data.model.subclass.CreneauLivraison;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Document(collection = "jourLivraisons")
public class JourLivraisonModel {

    @Id
    private String id;

    private LocalDate jour;

    Set<CreneauLivraison> creneauLivraisons = new HashSet<>();

    public JourLivraisonModel(LocalDate jour) {
        this.jour = jour;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDate getJour() {
        return jour;
    }

    public void setJour(LocalDate jour) {
        this.jour = jour;
    }

    public Set<CreneauLivraison> getCreneauLivraisons() {
        return creneauLivraisons;
    }

    public void setCreneauLivraisons(Set<CreneauLivraison> creneauLivraisons) {
        this.creneauLivraisons = creneauLivraisons;
    }

    public void addOrUpdateCreneauLivraison(CreneauLivraison creneau) {
        Set<CreneauLivraison> updatedCreneaux = this.creneauLivraisons;
        if (updatedCreneaux.contains(creneau)) {
            updatedCreneaux.remove(creneau);
        }
        updatedCreneaux.add(creneau);
        this.creneauLivraisons = updatedCreneaux;
    }
}
