package com.kata.delivery.data.enums;

public enum EStatusLivraison {
    EN_ATTENTE,
    EN_COURS,
    LIVREE,
    ANNULEE
}
