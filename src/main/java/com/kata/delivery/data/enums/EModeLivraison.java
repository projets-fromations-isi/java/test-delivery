package com.kata.delivery.data.enums;

public enum EModeLivraison {
    DRIVE,
    DELIVERY,
    DELIVERY_TODAY,
    DELIVERY_ASAP
}
