package com.kata.delivery.web.dto.request;

import com.kata.delivery.data.enums.EModeLivraison;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
public class JourLivraisonSearchRequestDTO {

    private EModeLivraison modeLivraison;

    private LocalDate jour;

    private LocalDate jourDebut;

    private LocalDate jourFin;

    private LocalTime heureDebut;

    private LocalTime heureFin;

    private List<String> livraisonIds;

    private Sort.Direction sortDirection = Sort.Direction.DESC;

    private String sortField = "jour";

    public EModeLivraison getModeLivraison() {
        return modeLivraison;
    }

    public void setModeLivraison(EModeLivraison modeLivraison) {
        this.modeLivraison = modeLivraison;
    }

    public LocalDate getJour() {
        return jour;
    }

    public void setJour(LocalDate jour) {
        this.jour = jour;
    }

    public LocalDate getJourDebut() {
        return jourDebut;
    }

    public void setJourDebut(LocalDate jourDebut) {
        this.jourDebut = jourDebut;
    }

    public LocalDate getJourFin() {
        return jourFin;
    }

    public void setJourFin(LocalDate jourFin) {
        this.jourFin = jourFin;
    }

    public LocalTime getHeureDebut() {
        return heureDebut;
    }

    public void setHeureDebut(LocalTime heureDebut) {
        this.heureDebut = heureDebut;
    }

    public LocalTime getHeureFin() {
        return heureFin;
    }

    public void setHeureFin(LocalTime heureFin) {
        this.heureFin = heureFin;
    }

    public List<String> getLivraisonIds() {
        return livraisonIds;
    }

    public void setLivraisonIds(List<String> livraisonIds) {
        this.livraisonIds = livraisonIds;
    }

    public Sort.Direction getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(Sort.Direction sortDirection) {
        this.sortDirection = sortDirection;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }
}
