package com.kata.delivery.web.dto.request;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@NoArgsConstructor
@AllArgsConstructor
public class CreneauLivraisonRequestDTO {

    @NotBlank(message = "La livraison est obligatoire")
    private String livraisonId;

    @NotBlank(message = "Le jour est obligatoire")
    private LocalDate jour;

    @NotBlank(message = "L'heure de début est obligatoire")
    private LocalTime heureDebut;

    @NotBlank(message = "L'heure de fin est obligatoire")
    private LocalTime heureFin;

    public String getLivraisonId() {
        return livraisonId;
    }

    public void setLivraisonId(String livraisonId) {
        this.livraisonId = livraisonId;
    }

    public LocalDate getJour() {
        return jour;
    }

    public void setJour(LocalDate jour) {
        this.jour = jour;
    }

    public LocalTime getHeureDebut() {
        return heureDebut;
    }

    public void setHeureDebut(LocalTime heureDebut) {
        this.heureDebut = heureDebut;
    }

    public LocalTime getHeureFin() {
        return heureFin;
    }

    public void setHeureFin(LocalTime heureFin) {
        this.heureFin = heureFin;
    }
}
