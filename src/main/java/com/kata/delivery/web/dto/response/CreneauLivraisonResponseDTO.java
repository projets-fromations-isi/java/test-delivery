package com.kata.delivery.web.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

public class CreneauLivraisonResponseDTO {

    private LivraisonResponseDTO livraison;

    private LocalDate jour;

    private LocalTime heureDebut;

    private LocalTime heureFin;

    public CreneauLivraisonResponseDTO() {
    }

    public CreneauLivraisonResponseDTO(LivraisonResponseDTO livraison, LocalDate jour, LocalTime heureDebut, LocalTime heureFin) {
        this.livraison = livraison;
        this.jour = jour;
        this.heureDebut = heureDebut;
        this.heureFin = heureFin;
    }

    public LivraisonResponseDTO getLivraison() {
        return livraison;
    }

    public void setLivraison(LivraisonResponseDTO livraison) {
        this.livraison = livraison;
    }

    public LocalDate getJour() {
        return jour;
    }

    public void setJour(LocalDate jour) {
        this.jour = jour;
    }

    public LocalTime getHeureDebut() {
        return heureDebut;
    }

    public void setHeureDebut(LocalTime heureDebut) {
        this.heureDebut = heureDebut;
    }

    public LocalTime getHeureFin() {
        return heureFin;
    }

    public void setHeureFin(LocalTime heureFin) {
        this.heureFin = heureFin;
    }
}
