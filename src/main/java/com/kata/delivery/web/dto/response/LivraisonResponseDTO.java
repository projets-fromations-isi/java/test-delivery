package com.kata.delivery.web.dto.response;

import com.kata.delivery.data.enums.EModeLivraison;
import com.kata.delivery.data.enums.EStatusLivraison;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class LivraisonResponseDTO {

    private String id;

    private String commandeId;

    private String clientId;

    private EModeLivraison modeLivraison;

    private String adresseLivraison;

    private String numeroSuivi;

    private EStatusLivraison statusLivraison;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCommandeId() {
        return commandeId;
    }

    public void setCommandeId(String commandeId) {
        this.commandeId = commandeId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public EModeLivraison getModeLivraison() {
        return modeLivraison;
    }

    public void setModeLivraison(EModeLivraison modeLivraison) {
        this.modeLivraison = modeLivraison;
    }

    public String getAdresseLivraison() {
        return adresseLivraison;
    }

    public void setAdresseLivraison(String adresseLivraison) {
        this.adresseLivraison = adresseLivraison;
    }

    public String getNumeroSuivi() {
        return numeroSuivi;
    }

    public void setNumeroSuivi(String numeroSuivi) {
        this.numeroSuivi = numeroSuivi;
    }

    public EStatusLivraison getStatusLivraison() {
        return statusLivraison;
    }

    public void setStatusLivraison(EStatusLivraison statusLivraison) {
        this.statusLivraison = statusLivraison;
    }
}
