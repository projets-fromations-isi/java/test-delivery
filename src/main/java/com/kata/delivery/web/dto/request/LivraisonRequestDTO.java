package com.kata.delivery.web.dto.request;

import com.kata.delivery.data.enums.EModeLivraison;
import com.kata.delivery.web.validators.IsEnumValue;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class LivraisonRequestDTO {

    @NotBlank(message = "La commande est obligatoire")
    private String commandeId;

    @NotBlank(message = "Le client est obligatoire")
    private String clientId;

    @IsEnumValue(enumClass = EModeLivraison.class, message = "le mode de livraison n'est pas dans les type de EModeLivraison")
    @NotBlank(message = "Le mode de livraison est obligatoire")
    private String modeLivraison;

    @NotBlank(message = "L'adresseLivraison est obligatoire. Soit une adresse pour te livrer soit un point de relais")
    private String adresseLivraison;

    public String getCommandeId() {
        return commandeId;
    }

    public void setCommandeId(String commandeId) {
        this.commandeId = commandeId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getModeLivraison() {
        return modeLivraison;
    }

    public void setModeLivraison(String modeLivraison) {
        this.modeLivraison = modeLivraison;
    }

    public String getAdresseLivraison() {
        return adresseLivraison;
    }

    public void setAdresseLivraison(String adresseLivraison) {
        this.adresseLivraison = adresseLivraison;
    }
}
