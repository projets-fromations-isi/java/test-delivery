package com.kata.delivery.web.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class CorsConfiguration implements WebMvcConfigurer {

  @Override
  public void addCorsMappings(CorsRegistry registry) {
    registry.addMapping("/**")
        .allowedOriginPatterns("http://127.0.0.1", "http://localhost", "http://127.0.0.1:*")
        .allowedMethods("GET", "POST", "PUT", "DELETE")
        .maxAge(8000L)
        .allowedHeaders("Origin", "Content-Type", "Accept")
        .allowCredentials(true);
  }
}
