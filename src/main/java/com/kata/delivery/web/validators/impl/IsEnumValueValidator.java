package com.kata.delivery.web.validators.impl;

import com.kata.delivery.web.validators.IsEnumValue;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.Arrays;

public class IsEnumValueValidator implements ConstraintValidator<IsEnumValue, String> {

    private Class<? extends Enum<?>> enumClass;

    @Override
    public void initialize(IsEnumValue context) {
        enumClass = context.enumClass();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return Arrays.stream(enumClass.getEnumConstants()).anyMatch(e -> e.name().equalsIgnoreCase(value));
    }
}
