package com.kata.delivery.web.controller;

import com.kata.delivery.data.mapper.LivraisonMapper;
import com.kata.delivery.data.model.LivraisonModel;
import com.kata.delivery.service.LivraisonService;
import com.kata.delivery.web.dto.request.CreneauLivraisonRequestDTO;
import com.kata.delivery.web.dto.request.LivraisonRequestDTO;
import com.kata.delivery.web.dto.response.CreneauLivraisonResponseDTO;
import com.kata.delivery.web.dto.response.LivraisonResponseDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/livraisons")
public class LivraisonController {

    private final LivraisonService livraisonService;

    private final LivraisonMapper livraisonMapper;

    public LivraisonController(LivraisonService livraisonService, LivraisonMapper livraisonMapper) {
        this.livraisonService = livraisonService;
        this.livraisonMapper = livraisonMapper;
    }

    @GetMapping("/list")
    public ResponseEntity<Page<LivraisonResponseDTO>> list(Pageable page) {
        Page<LivraisonResponseDTO> livraisons = livraisonService.list(page).map(livraisonModel -> livraisonMapper.toResponseDTO(livraisonModel));
        return ResponseEntity.ok(livraisons);
    }

    @GetMapping("/single/{id}")
    public ResponseEntity<LivraisonResponseDTO> single(@PathVariable String id) {
        LivraisonModel livraison = livraisonService.get(id);
        return ResponseEntity.ok(livraisonMapper.toResponseDTO(livraison));
    }

    @PostMapping("/create")
    public ResponseEntity<LivraisonResponseDTO> create(@RequestBody LivraisonRequestDTO requestDTO) {
        LivraisonModel livraison = livraisonService.create(requestDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(livraisonMapper.toResponseDTO(livraison));
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<LivraisonResponseDTO> update(@PathVariable String id, @RequestBody LivraisonRequestDTO requestDTO) {
        LivraisonModel livraison = livraisonService.update(id, requestDTO);
        return ResponseEntity.ok(livraisonMapper.toResponseDTO(livraison));
    }

    @GetMapping("/creneau/{livraisonId}")
    public ResponseEntity<CreneauLivraisonResponseDTO> getCreneauLivraison(@PathVariable String livraisonId) {
        CreneauLivraisonResponseDTO creneauLivraison = livraisonService.getCreneauLivraison(livraisonId);
        return ResponseEntity.ok(creneauLivraison);
    }

    @PostMapping("/creneau/choose")
    public ResponseEntity<CreneauLivraisonResponseDTO> chooseCreneauLivraison(@RequestBody CreneauLivraisonRequestDTO requestDTO) {
        CreneauLivraisonResponseDTO creneauLivraison = livraisonService.chooseCreneauLivraison(requestDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(creneauLivraison);
    }
}
