package com.kata.delivery.util;

import java.security.SecureRandom;

public class NumeroSuiviGenerator {

    private static final String ALPHANUMERIC_CHARACTERS = "ABCDEFGHJKMNPQRSTUVWXYZ123456789";
    private static final int NUMERIC_LENGTH = 11;
    private static final int ALPHANUMERIC_LENGTH = 2;

    private static final SecureRandom secureRandom = new SecureRandom();

    public static String genererNumeroSuivi() {
        String partieNumerique = genererPartieNumerique();
        String partieAlphanumerique = genererPartieAlphanumerique();

        // Utilisation de String.format pour garantir la longueur fixe
        String numeroSuivi = String.format("%s%s", partieNumerique, partieAlphanumerique);

        return numeroSuivi;
    }

    private static String genererPartieNumerique() {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < NUMERIC_LENGTH; i++) {
            int randomDigit = secureRandom.nextInt(10);
            sb.append(randomDigit);
        }

        return sb.toString();
    }

    private static String genererPartieAlphanumerique() {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < ALPHANUMERIC_LENGTH; i++) {
            int randomIndex = secureRandom.nextInt(ALPHANUMERIC_CHARACTERS.length());
            char randomChar = ALPHANUMERIC_CHARACTERS.charAt(randomIndex);
            sb.append(randomChar);
        }

        return sb.toString();
    }
}
