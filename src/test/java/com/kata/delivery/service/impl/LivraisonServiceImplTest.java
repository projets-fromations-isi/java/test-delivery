package com.kata.delivery.service.impl;

import com.kata.delivery.data.mapper.LivraisonMapper;
import com.kata.delivery.data.model.LivraisonModel;
import com.kata.delivery.data.repository.JourLivraisonRepository;
import com.kata.delivery.data.repository.LivraisonRepository;
import com.kata.delivery.web.dto.request.LivraisonRequestDTO;
import com.kata.delivery.web.exception.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class LivraisonServiceImplTest {

    @Mock
    private LivraisonRepository livraisonRepository;

    @Mock
    private JourLivraisonRepository jourLivraisonRepository;

    @Mock
    private LivraisonMapper livraisonMapper;

    @InjectMocks
    private LivraisonServiceImpl livraisonService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetLivraisonById() {
        String livraisonId = "livraisonId";
        LivraisonModel livraisonModel = new LivraisonModel();
        livraisonModel.setId(livraisonId);
        when(livraisonRepository.findById(livraisonId)).thenReturn(Optional.of(livraisonModel));

        LivraisonModel result = livraisonService.get(livraisonId);

        assertNotNull(result);
        assertEquals(livraisonModel, result);
    }

    @Test
    void testGetLivraisonByIdNotFound() {
        // Arrange
        String livraisonId = "livraisonId";
        when(livraisonRepository.findById(livraisonId)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> livraisonService.get(livraisonId));
    }

    @Test
    void testCreateLivraison() {
        // Arrange
        LivraisonRequestDTO requestDTO = new LivraisonRequestDTO();
        LivraisonModel livraisonModel = new LivraisonModel();
        when(livraisonMapper.toModel(requestDTO)).thenReturn(livraisonModel);

        // Act
        LivraisonModel result = livraisonService.create(requestDTO);

        // Assert
        assertNotNull(result);
        verify(livraisonMapper, times(1)).toModel(requestDTO);
        verify(livraisonRepository, times(1)).save(livraisonModel);
    }

    @Test
    void testUpdateLivraison() {
        // Arrange
        String livraisonId = "livraisonId";
        LivraisonRequestDTO requestDTO = new LivraisonRequestDTO();
        when(livraisonRepository.findById(livraisonId)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(EntityNotFoundException.class, () -> livraisonService.update(livraisonId, requestDTO));

        // Verify that findById was called
        verify(livraisonRepository, times(1)).findById(livraisonId);
        // Ensure that other methods were not called
        verify(livraisonMapper, never()).partialUpdate(any(), any());
        verify(livraisonRepository, never()).save(any());
    }

}
