package com.kata.delivery.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kata.delivery.data.mapper.LivraisonMapper;
import com.kata.delivery.data.model.LivraisonModel;
import com.kata.delivery.web.dto.request.CreneauLivraisonRequestDTO;
import com.kata.delivery.web.dto.request.LivraisonRequestDTO;
import com.kata.delivery.web.dto.response.CreneauLivraisonResponseDTO;
import com.kata.delivery.web.dto.response.LivraisonResponseDTO;
import com.kata.delivery.service.LivraisonService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
class LivraisonControllerTest {

    private MockMvc mockMvc;

    @Mock
    private LivraisonService livraisonService;

    @Mock
    private LivraisonMapper livraisonMapper;

    @InjectMocks
    private LivraisonController livraisonController;

    private final ObjectMapper objectMapper = new ObjectMapper();

    public void setUp() {
        MockitoAnnotations.openMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(livraisonController).build();
    }

    @Test
    void testListLivraisons() throws Exception {
        // Arrange
        Page<LivraisonModel> livraisons = new PageImpl<>(Collections.singletonList(new LivraisonModel()));
        when(livraisonService.list(any(Pageable.class))).thenReturn(livraisons);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/livraisons/list"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content").isArray());
    }

    @Test
    void testSingleLivraison() throws Exception {
        // Arrange
        LivraisonModel livraisonModel = new LivraisonModel();
        when(livraisonService.get(anyString())).thenReturn(livraisonModel);
        when(livraisonMapper.toResponseDTO(any())).thenReturn(new LivraisonResponseDTO());

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/livraisons/single/{id}", "1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").exists());
    }

    @Test
    void testCreateLivraison() throws Exception {
        // Arrange
        LivraisonRequestDTO requestDTO = new LivraisonRequestDTO();
        LivraisonModel livraisonModel = new LivraisonModel();
        when(livraisonService.create(any())).thenReturn(livraisonModel);
        when(livraisonMapper.toResponseDTO(any())).thenReturn(new LivraisonResponseDTO());

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/livraisons/create")
                        .content(objectMapper.writeValueAsString(requestDTO))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").exists());
    }

    @Test
    void testUpdateLivraison() throws Exception {
        // Arrange
        LivraisonRequestDTO requestDTO = new LivraisonRequestDTO();
        LivraisonModel livraisonModel = new LivraisonModel();
        when(livraisonService.update(anyString(), any())).thenReturn(livraisonModel);
        when(livraisonMapper.toResponseDTO(any())).thenReturn(new LivraisonResponseDTO());

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/livraisons/update/{id}", "1")
                        .content(objectMapper.writeValueAsString(requestDTO))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").exists());
    }

    @Test
    void testGetCreneauLivraison() throws Exception {
        // Arrange
        CreneauLivraisonResponseDTO creneauLivraison = new CreneauLivraisonResponseDTO();
        when(livraisonService.getCreneauLivraison(anyString())).thenReturn(creneauLivraison);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/livraisons/creneau/{livraisonId}", "1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.livraisonId").exists());
    }

    @Test
    void testChooseCreneauLivraison() throws Exception {
        // Arrange
        CreneauLivraisonRequestDTO requestDTO = new CreneauLivraisonRequestDTO();
        CreneauLivraisonResponseDTO creneauLivraison = new CreneauLivraisonResponseDTO();
        when(livraisonService.chooseCreneauLivraison(any())).thenReturn(creneauLivraison);

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/livraisons/creneau/choose")
                        .content(objectMapper.writeValueAsString(requestDTO))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.livraisonId").exists());
    }
}

